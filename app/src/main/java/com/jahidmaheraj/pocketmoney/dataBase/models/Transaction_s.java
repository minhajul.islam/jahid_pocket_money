package com.jahidmaheraj.pocketmoney.dataBase.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Transaction_s {
   @PrimaryKey(autoGenerate = true)
    int id_transaction;
    private int userId;
    private String date;
    private String time;
    private double amount;
    private String category;
    private String note;
    private String flag;

    public Transaction_s(int userId, String date, String time, double amount, String category, String note, String flag) {
        this.userId = userId;
        this.date = date;
        this.amount = amount;
        this.category = category;
        this.note = note;
        this.flag = flag;
        this.time = time;
    }

    public int getId_transaction() {
        return id_transaction;
    }

    public void setId_transaction(int id_transaction) {
        this.id_transaction = id_transaction;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
