package com.jahidmaheraj.pocketmoney.dataBase;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.jahidmaheraj.pocketmoney.dataBase.models.Category_s;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;

import java.util.List;

@Dao
public interface TransactionDAO {
    @Insert
    long insertExpanse(Transaction_s transaction);

    @Insert
    long insertDeposit(Transaction_s transaction);

    @Query("Select * from Transaction_s where userId=:id_user")
    List<Transaction_s> allTranaction(int id_user);

    @Query("Select * from Transaction_s where flag ='1' and userId=:id_user")
    List<Transaction_s> allExpense(int id_user);

    @Query("Select sum(amount) from Transaction_s where flag ='1' and userId=:id_user")
    float expenseAmount(int id_user);

    @Query("Select sum(amount) from Transaction_s where date >=:start AND date <=:end and flag ='1' and userId=:id_user")
    float expenseAmountOnMonth(String start, String end, int id_user);

    @Query("Select * from Transaction_s where flag ='2' and userId=:id_user")
    List<Transaction_s> allDeposit(int id_user);

    @Query("Select sum(amount) from Transaction_s where flag ='2' and userId=:id_user")
    float depositAmount(int id_user);

    @Query("Select sum(amount) from Transaction_s where  date >=:start AND date <=:end and flag ='2' and userId=:id_user")
    float depositAmountOnMonth(String start, String end, int id_user);


    @Query("SELECT * FROM Transaction_s where date >=:start AND date <=:end And userId =:id_user")
    List<Transaction_s> getTransactionRangOfDate(String start, String end, int id_user);


    @Query("SELECT * FROM Transaction_s where date >=:start AND date <=:end And category=:category And userId =:id_user")
    List<Transaction_s> getTransactionRangOfDateWithCategory(String start, String end, String category, int id_user);

    @Query("SELECT * FROM Transaction_s where date >=:start AND date <=:end And flag=:flag And userId =:id_user")
    List<Transaction_s> getTransactionRangOfDateWithTransactionType(String start, String end, String flag, int id_user);

    @Query("SELECT * FROM Transaction_s where date >=:start AND date <=:end and category=:category And flag=:flag And userId =:id_user")
    List<Transaction_s> getTransactionRangOfDateCategoryAandTransaction(String start, String end, String category, String flag, int id_user);


    /*Category Transaction*/
    @Insert
    long insertCategoryExppense(Category_s category);

    @Insert
    long insertCategoryDeposit(Category_s category);

    @Query("Select * from Category_s where categoryFlag='1'")
    List<Category_s> expanseCategory();

    @Query("Select * from Category_s where categoryFlag='2'")
    List<Category_s> depositCategory();

    @Query("Select DISTINCT categoryName from Category_s ")
    List<String> allCategory();

    @Query("DELETE FROM Transaction_s")
    void deleteTransaction();


}
