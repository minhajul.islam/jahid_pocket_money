package com.jahidmaheraj.pocketmoney.dataBase.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Category_s {
    @PrimaryKey(autoGenerate = true)
    int id_category;
    private String categoryName;
    private String categoryFlag;

    public Category_s(String categoryName, String categoryFlag) {
        this.categoryName = categoryName;
        this.categoryFlag = categoryFlag;
    }

    @Ignore
    public Category_s(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryFlag() {
        return categoryFlag;
    }

    public void setCategoryFlag(String categoryFlag) {
        this.categoryFlag = categoryFlag;
    }
}
