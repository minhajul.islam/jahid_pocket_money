package com.jahidmaheraj.pocketmoney.dataBase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseCategoryModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.IncomeCategoryModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.IncomeModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.UserModel;


@Database(entities = {UserModel.class, ExpenseModel.class, IncomeModel.class, IncomeCategoryModel.class, ExpenseCategoryModel.class},version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao dao();


}
