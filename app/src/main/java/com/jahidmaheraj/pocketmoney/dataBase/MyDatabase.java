package com.jahidmaheraj.pocketmoney.dataBase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

public class MyDatabase extends AppCompatActivity {
    AppDatabase Database = Room.databaseBuilder(getApplicationContext(),AppDatabase.class, "pocketmoney.db")
            .allowMainThreadQueries().build();

}
