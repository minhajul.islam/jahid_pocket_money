package com.jahidmaheraj.pocketmoney.dataBase.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "income_categories")
public class IncomeCategoryModel {
    //@Ignore
    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo(name = "category")
    private String category;

    public IncomeCategoryModel(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}