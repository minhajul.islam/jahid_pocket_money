package com.jahidmaheraj.pocketmoney.dataBase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.jahidmaheraj.pocketmoney.dataBase.models.Category_s;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;

@Database (entities = {Transaction_s.class, Category_s.class},version = 1,exportSchema = false)
public abstract class Database_s extends RoomDatabase {
    public abstract TransactionDAO transactionDAO();


}
