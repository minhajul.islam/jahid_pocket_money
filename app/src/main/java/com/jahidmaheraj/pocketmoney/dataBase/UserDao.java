package com.jahidmaheraj.pocketmoney.dataBase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseCategoryModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.IncomeCategoryModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.IncomeModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.UserModel;

import java.util.List;

@Dao
public interface UserDao {
    // User Account Operation-----------------------------------
    @Insert
    void insert(UserModel user);

    @Query("SELECT * FROM users WHERE name=:name AND password=:password")
    UserModel checkLogin(String name, String password);


    @Query("SELECT * FROM users")
    List<UserModel> getAll();


    // Expense Operation ----------------------------------------
    @Insert
    void insertExpense(ExpenseModel expense);

    @Query("SELECT * FROM expense where user_id= :id")
    List<ExpenseModel> getExpenses(int id);


    // Income Operation ----------------------------------------
    @Insert
    void insertIncome(IncomeModel income);

    @Query("SELECT * FROM income where user_id= :id")
    List<IncomeModel> getIncomes(int id);


    // Total income
    @Query("SELECT SUM(amount) FROM income where user_id= :id")
    double getTotalIncome(int id);

    // Total Expense
    @Query("SELECT SUM(amount) FROM expense where user_id= :id")
    double getTotalExpense(int id);

    // Add Category_s
    @Insert
    void addIncomeCategory(IncomeCategoryModel categoryModel);

    // Get Category_s
    @Query("SELECT DISTINCT category FROM income_categories")
    List<String> getIncomeCategory();



    // Add Expense Category_s
    @Insert
    void addExpenseCategory(ExpenseCategoryModel categoryModel);

    // Get Expense Category_s
    @Query("SELECT DISTINCT category FROM expense_categories")
    List<String> getExpenseCategory();

    // Get Daily income report
    @Query("SELECT * FROM income where date=:date AND user_id= :id")
    List<IncomeModel> getIncomeDaily(String date, int id);

    // Get Daily expense report
    @Query("SELECT * FROM expense where date=:date AND user_id= :id")
    List<ExpenseModel> getExpenseDaily(String date, int id);

    // Get Overall income report
    @Query("SELECT * FROM income where user_id= :id")
    List<IncomeModel> getIncomeAll(int id);

    // Get Overall expense report
    @Query("SELECT * FROM expense where user_id= :id")
    List<ExpenseModel> getExpenseAll(int id);

    // Get weekly expense report
    @Query("SELECT * FROM expense where date  = :date AND user_id= :id")
    ExpenseModel getExpenseWeekly(String date, int id);

    // Get weekly income report
    @Query("SELECT * FROM income where date  = :date AND user_id= :id")
    IncomeModel getIncomeWeekly(String date, int id);

    @Delete
    void deleteIncome(IncomeModel income);

    @Query("DELETE FROM expense")
    void deleteExpense();


    @Query("DELETE FROM expense")
    void clearDatabase();




}
