package com.jahidmaheraj.pocketmoney.dataBase.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "expense")
public class ExpenseModel {

    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo(name = "user_id")
    private int userId;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "amount")
    private double amount;

    @ColumnInfo(name = "cat")
    private String category;

    @ColumnInfo(name = "note")
    private String note;

    public ExpenseModel(int userId, String date, double amount, String category, String note) {
        this.userId = userId;
        this.date = date;
        this.amount = amount;
        this.category = category;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}

