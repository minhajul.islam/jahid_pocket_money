package com.jahidmaheraj.pocketmoney;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseModel;

import java.util.List;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ViewHolder> {

    List<ExpenseModel> list;

    public ExpenseAdapter(List<ExpenseModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycle, parent,false);

        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvDate.setText(list.get(position).getDate());
        holder.tvAmount.setText("" + list.get(position).getAmount());
        holder.tvCategory.setText(list.get(position).getCategory());
        holder.tvNote.setText(list.get(position).getNote());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate, tvAmount, tvCategory, tvNote;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate=itemView.findViewById(R.id.incomeReportDailyDate);
            tvAmount=itemView.findViewById(R.id.incomeReportDailyAmount);
            tvCategory=itemView.findViewById(R.id.incomeReportDailyCategory);
            tvNote=itemView.findViewById(R.id.incomeReportDailyNote);
        }
    }


}

