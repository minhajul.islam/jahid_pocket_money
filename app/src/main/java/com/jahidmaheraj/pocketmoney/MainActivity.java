package com.jahidmaheraj.pocketmoney;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;


import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jahidmaheraj.pocketmoney.fragment.FilterFragment;
import com.jahidmaheraj.pocketmoney.fragment.HomeFragment;
import com.jahidmaheraj.pocketmoney.fragment.SettingFragment;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottom_nav;
    private FrameLayout FL_container;
    private AdView mAdView;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Start with Home Fragment.....................
        getSupportFragmentManager().beginTransaction().replace(R.id.FL_container, new HomeFragment()).commit();

        //Initial Bottom Navigation..............................
        bottom_nav = findViewById(R.id.bottom_nav);
        bottom_nav.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bottom_nav.setSelectedItemId(R.id.home);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });





    }

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment setFragment = null;

            FragmentManager fragmentManager;

            switch (item.getItemId()) {
                case R.id.home:
                    setFragment = new HomeFragment();
                    break;
                case R.id.settings:
                    setFragment = new SettingFragment();
                    break;
                case R.id.filter:
                    setFragment = new FilterFragment();
                    break;
            }
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.FL_container, setFragment).commit();

            return true;
        }
    };


}
