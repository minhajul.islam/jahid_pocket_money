package com.jahidmaheraj.pocketmoney.fragment.homeOpetion;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.UserDao;
import com.jahidmaheraj.pocketmoney.dataBase.models.Category_s;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;
import com.jahidmaheraj.pocketmoney.fragment.FilterFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;


public class AddDepositFragment extends Fragment {
    private AppCompatImageButton btn_addCategory_deposit, btn_setDate_deposit, btn_setTime_deposit;
    private AppCompatButton btn_submit_deposit;
    private AppCompatEditText et_amount_deposit, et_note_deposit;
    private AppCompatTextView tv_date_deposit, tv_time_deposit;
    private ArrayAdapter cat_Adapter;
    private List<Category_s> cat_list;
    private AppCompatSpinner sp_category_deposit;
    private TransactionDAO transactionDAO;
    private Location location;
    private String selectedCategoy;

    private int user;

    private UserDao dao;

    private FragmentManager fragmentManager;

    public AddDepositFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_deposit, container, false);

        btn_addCategory_deposit = view.findViewById(R.id.btn_addCategory_deposit);
        btn_setDate_deposit = view.findViewById(R.id.btn_setDate_deposit);
        btn_setTime_deposit = view.findViewById(R.id.btn_setTime_deposit);
        btn_submit_deposit = view.findViewById(R.id.btn_submit_deposit);

        et_amount_deposit = view.findViewById(R.id.et_amount_deposit);
        et_note_deposit = view.findViewById(R.id.et_note_deposit);
        sp_category_deposit = view.findViewById(R.id.sp_category_deposit);
        tv_date_deposit = view.findViewById(R.id.tv_date_deposit);
        tv_time_deposit = view.findViewById(R.id.tv_time_deposit);


        setInstantDate();// set current DATE in tv_date_deposit
        setInstantTime();// set current TIME in tv_time_deposit


        Database_s database_s = Room.databaseBuilder(getContext(), Database_s.class, "samir")
                .allowMainThreadQueries()
                .build();
        transactionDAO = database_s.transactionDAO();

        //user id will be intializes from db

        user = 1;

        cat_list = new ArrayList<>();
        cat_list = transactionDAO.depositCategory();
        List<String> stringList = new ArrayList<>();
        for (Category_s c : cat_list) {
            stringList.add(c.getCategoryName());
        }
        cat_Adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, stringList);
        cat_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category_deposit.setAdapter(cat_Adapter);

        sp_category_deposit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCategoy = adapterView.getItemAtPosition(i).toString();
                //Toast.makeText(getContext(), selectedCategoy, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btn_setDate_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDatePicker();
            }
        });

        btn_setTime_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customTimePicker();
            }
        });

        btn_addCategory_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.FL_container, new AddDepositCagtegoryFragment());
                getFragmentManager().popBackStack();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btn_submit_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double t_deposit = Double.parseDouble(et_amount_deposit.getText().toString());
                    String t_date = tv_date_deposit.getText().toString();
                    String t_time = tv_time_deposit.getText().toString();
                    String t_note = et_note_deposit.getText().toString();

                    if (t_deposit>0 && !selectedCategoy.isEmpty() && !t_date.isEmpty() && !t_time.isEmpty()){
                        if (t_note.isEmpty()){
                            t_note="no comment";
                        }
                        Transaction_s transaction_s = new Transaction_s(user, t_date, t_time, t_deposit, selectedCategoy, t_note, "2");
                        transactionDAO.insertDeposit(transaction_s);
                        //Toast.makeText(getContext(), transactionDAO.allDeposit(user).size(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getContext(), "Deposit Added", Toast.LENGTH_SHORT).show();
                    }
                }catch (InputMismatchException e){
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    /*----------------------------Method--------------------------------*/

    private void customTimePicker() {
        Calendar time = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Date date = new Date(0, 0, 0, hourOfDay, minute);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                tv_time_deposit.setText(simpleDateFormat.format(date));
            }
        }, time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), false);

        timePickerDialog.show();
    }

    private void customDatePicker() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new Date(year - 1900, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                tv_date_deposit.setText(dateFormat.format(date));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void setInstantDate() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        tv_date_deposit.setText(strDate);
    }

    private void setInstantTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        tv_time_deposit.setText(simpleDateFormat.format(date));
    }

}
