package com.jahidmaheraj.pocketmoney.fragment;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.UserDao;
import com.jahidmaheraj.pocketmoney.fragment.settingOption.AboutUsFragment;
import com.jahidmaheraj.pocketmoney.fragment.settingOption.BackupFragment;
import com.jahidmaheraj.pocketmoney.fragment.settingOption.BudgetFragment;
import com.jahidmaheraj.pocketmoney.fragment.settingOption.ContactFragment;


public class SettingFragment extends Fragment {
    private AppCompatButton btn_contact_setting, btn_budget_setting, btn_backup_setting, btn_aboutUs_setting,  btn_clear_data;
    private FragmentManager fragmentManager;
    private TransactionDAO dao;
    private UserDao userdao;
    BackupFragment backupFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        btn_contact_setting = view.findViewById(R.id.btn_contact_setting);
       // btn_budget_setting = view.findViewById(R.id.btn_budget_setting);
        btn_backup_setting = view.findViewById(R.id.btn_backup_setting);
        btn_aboutUs_setting = view.findViewById(R.id.btn_aboutUs_setting);
        btn_clear_data = view.findViewById(R.id.btn_clear_data_setting);

        backupFragment = new BackupFragment();

      /*  btn_budget_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.FL_container, new BudgetFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/

        btn_clear_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder clearDataDialog = new AlertDialog.Builder(getContext());
                clearDataDialog.setTitle("Do you want to clear all data?");
                LinearLayout layout = new LinearLayout(getContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                Database_s database_s = Room.databaseBuilder (getContext (),Database_s.class,"samir")
                        .allowMainThreadQueries ()
                        .build ();


                clearDataDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        database_s.clearAllTables();
                        Toast.makeText(getContext(), "All data has been cleared", Toast.LENGTH_SHORT).show();

                    }
                });

                clearDataDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Toast.makeText(getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });

                clearDataDialog.show();





            }
        });

        btn_backup_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder adBackup = new AlertDialog.Builder(getContext());
                adBackup.setTitle("Data Backup");
                LinearLayout layout = new LinearLayout(getContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText dbName = new EditText(getContext());
                dbName.setInputType(InputType.TYPE_CLASS_TEXT);
                dbName.setHint("Give Your database name");
                layout.addView(dbName);

                adBackup.setView(layout);


// Set up the buttons
                adBackup.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getContext(), dbName.getText(), Toast.LENGTH_SHORT).show();

                    }
                });
                adBackup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Toast.makeText(getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });

                adBackup.show();


//                fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.FL_container, new BackupFragment());
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });


        btn_contact_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getSupportFragmentManager().beginTransaction().replace(R.id.FL_container,new HomeFragment()).commit();
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.FL_container, new ContactFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btn_aboutUs_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.FL_container, new AboutUsFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });




        return view;
    }

}
