package com.jahidmaheraj.pocketmoney.fragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.IncomeAdapter;
import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseModel;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends Fragment {
    private AppCompatTextView tv_dateStart_filter, tv_dateEnd_filter;
    private AppCompatSpinner sp_category_filter, sp_transaction_filter;
    private AppCompatButton btn_submitQuery_filter;
    private RecyclerView rv_filterResult_Filter;

    private ArrayAdapter categoryAdapter;
    private ArrayAdapter transactionTypeAdapter;
    private List<String> allCategoryList;
    private List<Transaction_s> transactions;
    private List<ExpenseModel> expenseModels;
    private TransactionDAO transactionDAO;
    private String[] t_type;
    private String selectForQueryCategoryName;
    private String selectForQueryTransactionType;

    public FilterFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        sp_category_filter = view.findViewById(R.id.sp_category_filter);
        sp_transaction_filter = view.findViewById(R.id.sp_transaction_filter);
        tv_dateStart_filter = view.findViewById(R.id.tv_dateStart_filter);
        tv_dateEnd_filter = view.findViewById(R.id.tv_dateEnd_filter);
        btn_submitQuery_filter = view.findViewById(R.id.btn_submitQuery_filter);
        rv_filterResult_Filter = view.findViewById(R.id.rv_filterResult_Filter);

        setInstantDate();


        Database_s database_s = Room.databaseBuilder(getContext(), Database_s.class, "samir")
                .allowMainThreadQueries()
                .build();
        transactionDAO = database_s.transactionDAO();


        transactions = new ArrayList<>();
        transactions = transactionDAO.allTranaction(1);
        Collections.reverse(transactions);

        /*Recycler View Set Data Default all Transaction_s*/
        recyclerViewMethod();


        /*---------------------------------------------*/


        tv_dateStart_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customStartDatePicker();
            }
        });

        tv_dateEnd_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customEndDatePicker();
            }
        });


        allCategoryList = new ArrayList<>();

        allCategoryList = transactionDAO.allCategory();
        allCategoryList.add("Select...");
        Collections.reverse(allCategoryList);
        categoryAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, allCategoryList);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category_filter.setAdapter(categoryAdapter);

        sp_category_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    selectForQueryCategoryName = adapterView.getItemAtPosition(i).toString();
                    Toast.makeText(getContext(), adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                } else {
                    selectForQueryCategoryName = "null";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        t_type = getResources().getStringArray(R.array.transactionType);
        transactionTypeAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, t_type);
        transactionTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_transaction_filter.setAdapter(transactionTypeAdapter);
        sp_transaction_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    selectForQueryTransactionType = String.valueOf(i);
                    Toast.makeText(getContext(), adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                } else {
                    selectForQueryTransactionType = "null";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_submitQuery_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String startDate = tv_dateStart_filter.getText().toString();
                String endDate = tv_dateEnd_filter.getText().toString();

                if (selectForQueryCategoryName.equals("null") && selectForQueryTransactionType.equals("null")) {
                    transactions = transactionDAO.getTransactionRangOfDate(startDate, endDate, 1);
                    Collections.reverse(transactions);
                    recyclerViewMethod();
                } else {
                    if (!selectForQueryCategoryName.isEmpty() && selectForQueryTransactionType.equals("null")) {
                        String category = selectForQueryCategoryName;
                        Toast.makeText(getContext(), selectForQueryCategoryName + selectForQueryTransactionType, Toast.LENGTH_SHORT).show();
                        transactions = transactionDAO.getTransactionRangOfDateWithCategory(startDate, endDate, category, 1);
                        Collections.reverse(transactions);
                        recyclerViewMethod();
                    } else if (selectForQueryCategoryName.equals("null") && !selectForQueryTransactionType.equals("null")) {
                        String transaction = selectForQueryTransactionType;
                        Toast.makeText(getContext(), selectForQueryCategoryName + selectForQueryTransactionType, Toast.LENGTH_SHORT).show();
                        transactions = transactionDAO.getTransactionRangOfDateWithTransactionType(startDate, endDate, transaction, 1);
                        Collections.reverse(transactions);
                        recyclerViewMethod();
                    } else {
                        String category = selectForQueryCategoryName;
                        String transaction = selectForQueryTransactionType;
                        Toast.makeText(getContext(), selectForQueryCategoryName + selectForQueryTransactionType, Toast.LENGTH_SHORT).show();
                        transactions = transactionDAO.getTransactionRangOfDateCategoryAandTransaction(startDate, endDate, category, transaction, 1);
                        Collections.reverse(transactions);
                        recyclerViewMethod();
                    }

                }
            }
        });
        return view;
    }

    private void recyclerViewMethod() {
        IncomeAdapter incomeAdapter = new IncomeAdapter(transactions);
        rv_filterResult_Filter.setAdapter(incomeAdapter);
        rv_filterResult_Filter.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    private void setInstantDate() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        tv_dateStart_filter.setText(strDate);
        tv_dateEnd_filter.setText(strDate);
    }

    private void customStartDatePicker() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new Date(year - 1900, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                tv_dateStart_filter.setText(dateFormat.format(date));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void customEndDatePicker() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new Date(year - 1900, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                tv_dateEnd_filter.setText(dateFormat.format(date));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

}
