package com.jahidmaheraj.pocketmoney.fragment.homeOpetion;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.UserDao;
import com.jahidmaheraj.pocketmoney.dataBase.models.Category_s;
import com.jahidmaheraj.pocketmoney.fragment.FilterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddExpenseCategoryFragment extends Fragment {
    private EditText edt_add_category;
    private Button btnSave;

    private UserDao dao;
    private TransactionDAO transactionDAO;
    private FragmentManager fragmentManager;


    public AddExpenseCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_dposit_cagtegory, container, false);
        edt_add_category = view.findViewById(R.id.edt_add_category);
        btnSave = view.findViewById(R.id.btnSave);
        // Inflate the layout for this fragment
        Database_s database_s = Room.databaseBuilder (getContext (),Database_s.class,"samir")
                .allowMainThreadQueries ()
                .build ();
        transactionDAO = database_s.transactionDAO ();
        // Inflate the layout for this fragment


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transactionDAO.insertCategoryExppense (new Category_s (edt_add_category.getText ().toString (),"1"));
                fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.FL_container,new AddExpenseFragment());
                getFragmentManager().popBackStack();
                transaction.addToBackStack(null);
                transaction.commit();



            }
        });


        return view;
    }

}
