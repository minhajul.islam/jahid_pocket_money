package com.jahidmaheraj.pocketmoney.fragment;


import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.fragment.homeOpetion.AddDepositFragment;
import com.jahidmaheraj.pocketmoney.fragment.homeOpetion.AddExpenseFragment;
import com.jahidmaheraj.pocketmoney.fragment.homeOpetion.AllDepositFragment;
import com.jahidmaheraj.pocketmoney.fragment.homeOpetion.AllExpenseFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;


public class HomeFragment extends Fragment {

    private FragmentManager fragmentManager;
    private PieChart piChart_home;
    private CardView all_deposit_BT, all_expense_BT;
    private TransactionDAO transactionDAO;
    private AppCompatTextView tv_totalExpanse_home, tv_totalDeposit_home;


    float deposit = 0, expense = 0, netDeposit = 0;
    String result = "problem";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.fragment_home, container, false);

        CardView btn_addDeposit_home = view.findViewById (R.id.btn_addDeposit_home);
        CardView btn_addExpense_home = view.findViewById (R.id.btn_addExpense_home);
        all_deposit_BT = view.findViewById (R.id.all_deposit_BT);
        all_expense_BT = view.findViewById (R.id.all_expense_BT);
        piChart_home = view.findViewById (R.id.piChart_home);
        tv_totalExpanse_home = view.findViewById (R.id.TV_total_expanse);
        tv_totalDeposit_home = view.findViewById (R.id.TV_total_deposit);


        Database_s database_s = Room.databaseBuilder (getContext (), Database_s.class, "samir")
                .allowMainThreadQueries ()
                .build ();
        transactionDAO = database_s.transactionDAO ();

        float expenseAmount = transactionDAO.expenseAmount (1);
        float depositAmount = transactionDAO.depositAmount (1);
        tv_totalExpanse_home.setText ("Total Expanse: " + expenseAmount);
        tv_totalDeposit_home.setText ("Total Deposit: " + depositAmount + "");

        pieChart ();// Pi-Chart is implemented using this method

        btn_addDeposit_home.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Log.d ("res", "onClick: ");
                AddDepositFragment add_deposit_fragment = new AddDepositFragment ();
                fragmentManager = getFragmentManager ();
                FragmentTransaction transaction = fragmentManager.beginTransaction ();
                transaction.replace (R.id.FL_container, add_deposit_fragment);
                getFragmentManager ().popBackStack ();
                transaction.addToBackStack (null);
                transaction.commit ();
            }
        });
        all_deposit_BT.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Log.d ("res", "onClick: ");
                AllDepositFragment all_deposit_fragment = new AllDepositFragment ();
                fragmentManager = getFragmentManager ();
                FragmentTransaction transaction = fragmentManager.beginTransaction ();
                transaction.replace (R.id.FL_container, all_deposit_fragment);
                getFragmentManager ().popBackStack ();
                transaction.addToBackStack (null);
                transaction.commit ();
            }
        });
        all_expense_BT.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Log.d ("res", "onClick: ");
                AllExpenseFragment all_expense_fragment = new AllExpenseFragment ();
                fragmentManager = getFragmentManager ();
                FragmentTransaction transaction = fragmentManager.beginTransaction ();
                transaction.replace (R.id.FL_container, all_expense_fragment);
                getFragmentManager ().popBackStack ();
                transaction.addToBackStack (null);
                transaction.commit ();
            }
        });
        btn_addExpense_home.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                AddExpenseFragment addDepositFragment = new AddExpenseFragment ();
                fragmentManager = getFragmentManager ();
                FragmentTransaction transactio2n = fragmentManager.beginTransaction ();
                transactio2n.replace (R.id.FL_container, addDepositFragment);
                getFragmentManager ().popBackStack ();
                transactio2n.addToBackStack (null);
                transactio2n.commit ();
            }
        });

        return view;
    }


    private void pieChart() {
        piChart_home.setUsePercentValues(true);
        piChart_home.getDescription().setEnabled(false);
        piChart_home.setExtraOffsets(5, 10, 5, 5);
        float deposit = 0,expense=0,netDeposit=0;
        String result="problem";
        try {
            //Toast.makeText(getContext(), "call", Toast.LENGTH_SHORT).show();
            deposit = transactionDAO.depositAmount(1);
            expense = transactionDAO.expenseAmount(1);

            netDeposit = deposit-expense;
            result = Float.toString(netDeposit);
            //Toast.makeText(getContext(), "unSuccess", Toast.LENGTH_SHORT).show();
        } catch (InputMismatchException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        piChart_home.setDragDecelerationFrictionCoef(1f);
        piChart_home.setDrawHoleEnabled(true);//for middle Hole
        piChart_home.setHoleColor(Color.WHITE);
        piChart_home.setCenterText(result);
        piChart_home.setCenterTextSize(20f);
        piChart_home.setTransparentCircleRadius(61f);

        ArrayList<PieEntry> yValues = new ArrayList<>();
        yValues.add(new PieEntry(deposit, "Deposit"));
        yValues.add(new PieEntry(expense, "Expense"));


        PieDataSet dataSet = new PieDataSet(yValues, "Transaction");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData pieData = new PieData(dataSet);
        pieData.setValueTextSize(10f);
        pieData.setValueTextColor(Color.YELLOW);

        piChart_home.setData(pieData);

    }

    private void monthlyValue() {
        Date date = new Date ();
        //Calendar gc = new GregorianCalendar();
        Calendar gc = Calendar.getInstance ();
        gc.setTime (date);
        gc.set (Calendar.MONTH, Calendar.MONTH);
        gc.set (Calendar.DAY_OF_MONTH, 1);
        Date monthStart = gc.getTime ();
        gc.add (Calendar.MONTH, 1);
        gc.add (Calendar.DAY_OF_MONTH, -1);
        Date monthEnd = gc.getTime ();
        SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd");

        /*System.out.println("Calculated month start date : " + format.format(monthStart));
        System.out.println("Calculated month end date : " + format.format(monthEnd));*/
        //transactions = transactionDAO.getTransactionRangOfDate(monthStart, monthEnd, 1);
        //tv_startDate_budget.setText(format.format(monthStart));
        //tv_endDate_budget.setText(format.format(monthEnd));
    }

}
