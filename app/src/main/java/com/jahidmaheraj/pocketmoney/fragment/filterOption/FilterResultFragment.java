package com.jahidmaheraj.pocketmoney.fragment.filterOption;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.AppDatabase;
import com.jahidmaheraj.pocketmoney.dataBase.UserDao;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterResultFragment extends Fragment {
    private ArrayAdapter cat_Adapter;
    private List<String> cat_list;
    private Spinner spCategory;
    private UserDao dao;

    public FilterResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate (R.layout.fragment_filter_result, container, false);
        AppDatabase Database = Room.databaseBuilder (getContext (), AppDatabase.class, "pocket_money.db")
                .allowMainThreadQueries ().build ();

        dao = Database.dao ();
        cat_list = new ArrayList<> ();
        cat_list = dao.getIncomeCategory ();

        cat_Adapter = new ArrayAdapter (getContext (), android.R.layout.simple_spinner_item, cat_list);
        cat_Adapter.setDropDownViewResource (android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter (cat_Adapter);
        return view;
    }

}
