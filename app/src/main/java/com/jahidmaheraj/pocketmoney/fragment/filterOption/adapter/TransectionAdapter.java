package com.jahidmaheraj.pocketmoney.fragment.filterOption.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.models.ExpenseModel;

import java.util.List;

public class TransectionAdapter extends RecyclerView.Adapter<TransectionAdapter.TransactionHolder> {
    private List<ExpenseModel> transactions;

    public TransectionAdapter(List<ExpenseModel> transactions) {
        this.transactions = transactions;
    }


    @NonNull
    @Override
    public TransactionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.transection_row,parent,false);
        return new TransactionHolder (itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionHolder holder, int position) {
        holder.et_name.setText (transactions.get(position).getCategory());
        holder.et_amount.setText (String.valueOf(transactions.get (position).getAmount())+" TAKA");
        holder.et_date.setText ("Date: "+transactions.get(position).getDate());
        holder.et_time.setText ("Time: "+transactions.get(position).getNote());
    }

    @Override
    public int getItemCount() {
        return transactions.size ();
    }

    public class TransactionHolder extends RecyclerView.ViewHolder{
        private MaterialTextView et_name,et_amount,et_date,et_time,et_description;
        public TransactionHolder(@NonNull View itemView) {
            super (itemView);
            et_name = itemView.findViewById (R.id.tv_transactionName);
            et_amount = itemView.findViewById (R.id.tv_transactionAmount);
            et_date = itemView.findViewById (R.id.tv_transactionDate);
            et_time = itemView.findViewById (R.id.tv_transactionTime);
            et_description = itemView.findViewById (R.id.tv_transactionDescription);
        }
    }


}
