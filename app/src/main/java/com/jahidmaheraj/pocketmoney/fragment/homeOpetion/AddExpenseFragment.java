package com.jahidmaheraj.pocketmoney.fragment.homeOpetion;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.UserDao;
import com.jahidmaheraj.pocketmoney.dataBase.models.Category_s;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;
import com.jahidmaheraj.pocketmoney.fragment.FilterFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;


public class AddExpenseFragment extends Fragment {
    private AppCompatImageButton btn_addExpCat_expense,btn_setDate_expense,btn_setTime_expense;
    private AppCompatTextView tv_date_expense, tv_time_expense;
    private AppCompatEditText et_amount_expense, et_note_expense;
    private Spinner sp_category_expense;
    private AppCompatButton btn_submit_expense;
    private ArrayAdapter cat_Adapter;
    private List<Category_s> cat_list;
    private TransactionDAO transactionDAO;
    private String t_selectCategory;

    // it will be fetched from user table
    private int user = 1;

    private UserDao dao;


    private FragmentManager fragmentManager;

    public AddExpenseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_expense, container, false);


        et_amount_expense = view.findViewById(R.id.et_amount_expense);
        sp_category_expense = view.findViewById(R.id.sp_category_expense);
        btn_addExpCat_expense = view.findViewById(R.id.btn_addExpCat_expense);
        tv_date_expense = view.findViewById(R.id.tv_date_expense);
        tv_time_expense = view.findViewById(R.id.tv_time_expense);
        et_note_expense = view.findViewById(R.id.et_note_expense);
        btn_submit_expense = view.findViewById(R.id.btn_submit_expense);
        btn_setTime_expense = view.findViewById(R.id.btn_setTime_expense);
        btn_setDate_expense = view.findViewById(R.id.btn_setDate_expense);

        setInstantDate();
        setInstantTime();

        Database_s database_s = Room.databaseBuilder(getContext(), Database_s.class, "samir")
                .allowMainThreadQueries()
                .build();
        transactionDAO = database_s.transactionDAO();

        cat_list = new ArrayList<>();
        cat_list = transactionDAO.expanseCategory();

        List<String> stringList = new ArrayList<>();
        for (Category_s s : cat_list) {
            stringList.add(s.getCategoryName());
        }
        cat_Adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, stringList);
        cat_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category_expense.setAdapter(cat_Adapter);
        sp_category_expense.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                t_selectCategory = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getContext(), t_selectCategory, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btn_setDate_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDatePicker();
            }
        });

        btn_setTime_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customTimePicker();
            }
        });


        btn_submit_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double t_amount = Double.parseDouble(et_amount_expense.getText().toString());
                    String t_date = tv_date_expense.getText().toString();
                    String t_time = tv_time_expense.getText().toString();
                    String t_note = et_note_expense.getText().toString();

                    if (t_amount > 0 && !t_selectCategory.isEmpty() && !t_date.isEmpty() && !t_time.isEmpty()) {
                        if (t_note.isEmpty()) {
                            t_note = "NoComment";
                        }
                        Transaction_s transaction_s = new Transaction_s(user, t_date, t_time, t_amount, t_selectCategory, t_note, "1");
                        transactionDAO.insertExpanse(transaction_s);
                        Toast.makeText(getContext(), String.valueOf(transactionDAO.allExpense(user).size()), Toast.LENGTH_SHORT).show();

                    }


                } catch (InputMismatchException e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });


        btn_addExpCat_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.FL_container, new AddExpenseCategoryFragment());
                getFragmentManager().popBackStack();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return view;
    }

    private void setInstantDate() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        tv_date_expense.setText(strDate);
    }

    private void setInstantTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        tv_time_expense.setText(simpleDateFormat.format(date));
    }


    //Time......................
    private void customDatePicker() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new Date(year - 1900, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                tv_date_expense.setText(dateFormat.format(date));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    //Calender...............
    private void customTimePicker() {
        Calendar time = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Date date = new Date(0, 0, 0, hourOfDay, minute);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                tv_time_expense.setText(simpleDateFormat.format(date));
            }
        }, time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), false);

        timePickerDialog.show();
    }

}


