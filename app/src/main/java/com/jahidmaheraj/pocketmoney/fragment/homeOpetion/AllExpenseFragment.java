package com.jahidmaheraj.pocketmoney.fragment.homeOpetion;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.IncomeAdapter;
import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;

import java.util.ArrayList;
import java.util.List;

public class AllExpenseFragment extends Fragment {
    private RecyclerView recyclerExpense;
    private List<Transaction_s> expenseModelList;
    private TransactionDAO transactionDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_expense, container, false);

        recyclerExpense = view.findViewById(R.id.recyclerIncome);
        expenseModelList = new ArrayList<>();

        Database_s database_s = Room.databaseBuilder (getContext (), Database_s.class, "samir")
                .allowMainThreadQueries ()
                .build ();
        transactionDAO = database_s.transactionDAO ();
        expenseModelList = transactionDAO.allExpense(1);
        Toast.makeText(getContext(), expenseModelList.size()+"", Toast.LENGTH_SHORT).show();

        IncomeAdapter adapter=new IncomeAdapter(expenseModelList);
        recyclerExpense.setAdapter(adapter);
        recyclerExpense.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }
}