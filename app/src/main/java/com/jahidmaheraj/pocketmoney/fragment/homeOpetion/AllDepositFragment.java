package com.jahidmaheraj.pocketmoney.fragment.homeOpetion;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jahidmaheraj.pocketmoney.IncomeAdapter;
import com.jahidmaheraj.pocketmoney.R;
import com.jahidmaheraj.pocketmoney.dataBase.Database_s;
import com.jahidmaheraj.pocketmoney.dataBase.TransactionDAO;
import com.jahidmaheraj.pocketmoney.dataBase.models.Transaction_s;

import java.util.ArrayList;
import java.util.List;


public class AllDepositFragment extends Fragment {
    private RecyclerView recyclerIncome, recyclerExpense;
    private List<Transaction_s> incomeModelList;
    private TransactionDAO transactionDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_deposit, container, false);

        recyclerIncome = view.findViewById(R.id.recyclerIncome);
        incomeModelList = new ArrayList<>();


        Database_s database_s = Room.databaseBuilder(getContext(), Database_s.class, "samir")
                .allowMainThreadQueries()
                .build();
        transactionDAO = database_s.transactionDAO();
        incomeModelList = transactionDAO.allDeposit(1);

        IncomeAdapter adapter = new IncomeAdapter(incomeModelList);
        recyclerIncome.setAdapter(adapter);
        recyclerIncome.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;
    }

}
