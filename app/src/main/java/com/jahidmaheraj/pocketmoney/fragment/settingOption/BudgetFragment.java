package com.jahidmaheraj.pocketmoney.fragment.settingOption;


import android.app.DatePickerDialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.jahidmaheraj.pocketmoney.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetFragment extends Fragment {
    private AppCompatTextView tv_startDate_budget, tv_endDate_budget;
    private AppCompatEditText et_budgetAmount_budget;
    private AppCompatButton btn_submit_budget;
    public static final String SHARED_PREFS = "DateStore";

    public BudgetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate (R.layout.fragment_budget, container, false);
        tv_startDate_budget = view.findViewById (R.id.tv_startDate_budget);
        tv_endDate_budget = view.findViewById (R.id.tv_endDate_budget);
        et_budgetAmount_budget = view.findViewById (R.id.et_budgetAmount_budget);
        btn_submit_budget = view.findViewById (R.id.btn_submit_budget);

        setInstantDate ();// set current Date

        tv_startDate_budget.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                customStartDatePicker ();
            }
        });

        tv_endDate_budget.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                customEndDatePicker ();
            }
        });

        btn_submit_budget.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if (checkValidation ()){
                    saveDatt ();// sharedPreferance
                }



            }
        });

        return view;
    }

    private boolean checkValidation() {
        double amount = Double.parseDouble (et_budgetAmount_budget.getText ().toString ());
        String startDate = tv_startDate_budget.getText ().toString ();
        String endDate = tv_endDate_budget.getText ().toString ();
        if (amount>0 && !startDate.isEmpty () && !endDate.isEmpty ()){
            return true;
        }else {
            tv_startDate_budget.setError ("set Date");
            tv_endDate_budget.setError ("set Date");
            et_budgetAmount_budget.setError ("Enter Budget");
            return false;
        }
    }

    private void saveDatt() {
        SharedPreferences sharedpreferences = getContext ().getSharedPreferences (SHARED_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit ();

        editor.putString ("start_date",tv_startDate_budget.getText ().toString ());
        editor.putString ("end_date",tv_endDate_budget.getText ().toString ());
        editor.putString ("budget",et_budgetAmount_budget.getText ().toString ());
        Toast.makeText (getContext (), "yeap!!", Toast.LENGTH_SHORT).show ();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getContext ().getSharedPreferences (SHARED_PREFS,Context.MODE_PRIVATE);

    }

    private void setInstantDate() {
        Date date = new Date ();
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd");
        String strDate = formatter.format (date);
        tv_startDate_budget.setText (strDate);
        tv_endDate_budget.setText (strDate);
    }

    private void customStartDatePicker() {
        Calendar calendar = Calendar.getInstance ();
        DatePickerDialog datePickerDialog = new DatePickerDialog (getContext (), new DatePickerDialog.OnDateSetListener () {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new Date (year - 1900, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
                tv_startDate_budget.setText (dateFormat.format (date));
            }
        }, calendar.get (Calendar.YEAR), calendar.get (Calendar.MONTH), calendar.get (Calendar.DAY_OF_MONTH));
        datePickerDialog.show ();
    }

    private void customEndDatePicker() {
        Calendar calendar = Calendar.getInstance ();
        DatePickerDialog datePickerDialog = new DatePickerDialog (getContext (), new DatePickerDialog.OnDateSetListener () {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new Date (year - 1900, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
                tv_endDate_budget.setText (dateFormat.format (date));
            }
        }, calendar.get (Calendar.YEAR), calendar.get (Calendar.MONTH), calendar.get (Calendar.DAY_OF_MONTH));
        datePickerDialog.show ();
    }
}
