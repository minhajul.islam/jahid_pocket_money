package com.jahidmaheraj.pocketmoney.splashScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.jahidmaheraj.pocketmoney.MainActivity;
import com.jahidmaheraj.pocketmoney.R;

public class splashScreen extends AppCompatActivity {

    private Animation topAnim,bottomAnim;
    private ImageView icon,lower;

    private static final int spalsh_screen = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);

        icon = findViewById(R.id.icon);
        lower = findViewById(R.id.lower);

        icon.setAnimation(topAnim);
        icon.setAnimation(bottomAnim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(splashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },spalsh_screen);


    }
}
