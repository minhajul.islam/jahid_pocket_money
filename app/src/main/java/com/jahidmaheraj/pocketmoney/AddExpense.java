package com.jahidmaheraj.pocketmoney;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class AddExpense extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Add Expense");

        setContentView(R.layout.activity_add_expense);
    }
}
